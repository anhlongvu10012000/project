import { NgModule ,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DonHopDongComponent } from './don-hop-dong/don-hop-dong.component';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { DonKhongHopDongComponent } from './don-khong-hop-dong/don-khong-hop-dong.component';
import { DonKhuyenMaiComponent } from './don-khuyen-mai/don-khuyen-mai.component';
import { ChiTietHoaDonComponent } from './chi-tiet-hoa-don/chi-tiet-hoa-don.component';
import { DonSanPhamMoiComponent } from './don-san-pham-moi/don-san-pham-moi.component';
import { ThongtinkhachhangComponent } from './thong-tin-khach-hang/thong-tin-khach-hang.component';
import { CeativeOrderComponent } from './ceative-order/ceative-order.component';
import { MenuComponent } from './menu/menu.component';





registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    DonHopDongComponent,
    DonKhongHopDongComponent,
    DonKhuyenMaiComponent,
    ChiTietHoaDonComponent,
    DonSanPhamMoiComponent,
    ThongtinkhachhangComponent,
    CeativeOrderComponent,
    MenuComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NzLayoutModule,
    NzBreadCrumbModule,
    NzIconModule,
    NzMenuModule,
    NzButtonModule,
    NzCollapseModule,
    NzGridModule,
    NzFormModule,
    NzSelectModule,
    NzInputModule,
    NzTableModule,
    NzCheckboxModule,
    NzDatePickerModule,
    NzTabsModule,
    NzModalModule,


  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

  bootstrap: [AppComponent],
})
export class AppModule { }
