import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuDropDownTokenFactory } from 'ng-zorro-antd/menu';
import { CeativeOrderComponent } from './ceative-order/ceative-order.component';
import { MenuComponent } from './menu/menu.component';
const routes: Routes = [

  {
     path: "",
     component: CeativeOrderComponent
  },
  {
    path:"",
    component: MenuComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
