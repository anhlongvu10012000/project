import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CeativeOrderComponent } from './ceative-order.component';

describe('CeativeOrderComponent', () => {
  let component: CeativeOrderComponent;
  let fixture: ComponentFixture<CeativeOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CeativeOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CeativeOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
