import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonSanPhamMoiComponent } from './don-san-pham-moi.component';

describe('DonSanPhamMoiComponent', () => {
  let component: DonSanPhamMoiComponent;
  let fixture: ComponentFixture<DonSanPhamMoiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonSanPhamMoiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonSanPhamMoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
